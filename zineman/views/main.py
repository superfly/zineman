from flask import Blueprint, render_template

from zineman.models import Page


main = Blueprint('main', __name__)


@main.route('/', methods=['GET'])
def index():
    pages = Page.query.all()
    return render_template('index.html', pages=pages)


@main.route('/pages/<slug>', methods=['GET'])
def pages(slug):
    page = Page.query.filter_by(slug=slug).first()
    pages = Page.query.all()
    return render_template('page.html', page=page, pages=pages)
